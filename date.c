/*
Parsa Bagheri
ID: 951406684
CIS 415 Project0: website_monitoring

	This is my work; however, using sscanf was suggested by professor Joe Sventek
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "date.h"

typedef struct dmy{
	int day;
	int month;
	int year;
}day_month_year;


static int date_compare(const Date *date1, const Date *date2){
	day_month_year *dmy1 = (day_month_year *)(date1->self);
	day_month_year *dmy2 = (day_month_year *)(date2->self);

	/*comparing the years*/
	int year_diff = dmy1->year - dmy2->year;
	if(year_diff < 0){
		return -1;
	}
	else if(year_diff == 0){

		/*comparing the months*/
		int month_diff = dmy1->month - dmy2->month;
		if(month_diff < 0){
			return -1;
		}
		else if(month_diff == 0){

			/*comparing the days*/
			int day_diff = dmy1->day - dmy2->day;
			if(day_diff < 0){
				return -1;
			}
			else if(day_diff == 0){
				return 0;
			}
		}
	}
	return 1;
}

static void date_destroy(const Date *d){
	if(d != NULL){
		day_month_year *dmy = (day_month_year *)(d->self);
		if(dmy != NULL){
			free(dmy);
		}
		free((void *)d);
	}
}

static const Date * date_duplicate(const Date *d){
	Date *date = (Date *)malloc(sizeof(Date));
	if(date != NULL){
		day_month_year *dmy = (day_month_year *)malloc(sizeof(day_month_year));
		if(dmy != NULL){
			day_month_year *dmy_dup = (day_month_year *)(d->self);
			dmy->day = dmy_dup->day; 
			dmy->month = dmy_dup->month;
			dmy->year = dmy_dup->year;
			
			date->self = (void *)dmy;
			date->duplicate = date_duplicate;
			date->compare = date_compare;
			date->destroy = date_destroy;
		}
		else{
			free(dmy);
			return NULL;
		}
		return date;
	}
	else
		return NULL;
}
const Date *Date_create(char *datestr){
	Date *date = (Date *)malloc(sizeof(Date));
	if (date != NULL){
		day_month_year *dmy = (day_month_year *)malloc(sizeof(day_month_year));
		if(dmy != NULL){

			// /*creating char arrays to hold vlaues of day, month and year*/
			// char day[2], month[3], year[5];
			// day[2]=month[2]=year[4]='\0';

			// /*populating day, month and year*/
			// strncpy(day, datestr, 2);
			// strncpy(month, datestr+3, 2);
			// strncpy(year, datestr+6, 4);

			// /*converting day, month and year to int*/
			// dmy->day = atoi(day);
			// dmy->month = atoi(month);
			// dmy->year = atoi(year);

			/*better way*/
			char d[11];
			strcpy(d, datestr);

			sscanf(d, "%d/%d/%d", &(dmy->day), &(dmy->month), &(dmy->year));

			date->duplicate = date_duplicate;
			date->compare = date_compare;
			date->destroy = date_destroy;
			date->self = (void *)dmy;
		}
		else{
			free(date);
			date = NULL;
		}
	}
	return date;
}