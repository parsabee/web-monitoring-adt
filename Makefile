CFLAGS= -W -Wall -g
PROGS= tldmonitor
OBJECTS=date.o tldmap.o iterator.o tldmonitor.o

all: $(PROGS)

tldmonitor: tldmonitor.o date.o tldmap.o iterator.o
	cc -o tldmonitor $^

tldmonitor.o: tldmonitor.c date.h tldmap.h
date.o: date.h date.c
tldmap.o: tldmap.c tldmap.h date.h
iterator.o: iterator.c iterator.h

clean:
	rm -f *~ $(OBJECTS) $(PROGS)
