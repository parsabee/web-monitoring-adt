/*
Parsa Bagheri
ID: 951406684
CIS 415 Project0: website_monitoring

	TLDMap implements a binary search tree structure. This is my work; however, some data structure design algorithms
	for creating the iterator are taken form stack.c provided by professor Joe Sventek
*/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "tldmap.h"
#include "iterator.h"

#define MIN_CAPACITY 50L

TLDNode *root = NULL;

struct tldnode
{	
	TLDNode *p; /*parent*/
	TLDNode *lc; /*left child*/
	TLDNode *rc; /*right child*/
	char *k; /*key: top-level-domain*/
	long v; /*value*/
};

/*following struct is for keeping track of size and elements of TLDMap as it grows*/
typedef struct elms{
	long size;
	long capacity;
	void **elements;
}Elements;

/*helper function to free all the nodes in the bst*/
static void destroyNodes(TLDNode *node){
	if(node->lc != NULL){
		destroyNodes(node->lc);
	}
	if(node->rc != NULL){
		destroyNodes(node->rc);
	}
	free(node->k);
	free(node);
}

static void bst_destroy(const TLDMap *tld){
	Elements *elm = (Elements *)(tld->self);

	/*starting with the root, recursively destroy all of its children down to the leaves*/
	destroyNodes(root);

	/*now that all of the nodes are freed we can free the structures*/
	free(elm);
	free((void *)tld);
}

static int bst_insert(const TLDMap *tld, char *theTLD, long v){
	char *tldcpy = strdup(theTLD);
	if(tldcpy == NULL){
		fprintf(stderr, "Failed to copy theTLD on the heap\n");
		return 0;
	}
	Elements *elms = (Elements *)(tld->self);
	if(elms != NULL){

		/*
		before inserting new nodes, we need to check if we have enough space,
		if not, we need to reallocate space.
		*/
		if(elms->size >= elms->capacity){
			size_t n_bytes = 2*(elms->capacity)*sizeof(TLDNode);

			void **tmp = (void **)realloc((elms->elements), n_bytes);
			if(tmp != NULL){
				elms->elements = tmp;
				elms->capacity = n_bytes;
			}
			else{
				fprintf(stderr, "Elements array is full, memory reallocation failed\n");
				free(tldcpy);
				return 0;
			}
		}

		/*if the size is 0, we are inserting the root of the tree*/
		if(elms->size == 0L){

			root = (TLDNode *)malloc(sizeof(TLDNode));
			if(root != NULL){
				root->k = tldcpy;
				root->v = v;
				root->lc = NULL;
				root->rc = NULL;
				root->p = NULL;

				elms->elements[elms->size] = root;
				elms->size += 1L;
			}
			else{
				fprintf(stderr, "Failed to allocate memory to root\n");
				free(tldcpy);
				return 0;
			}
		}

		/*if size is >0, then the inserting node is not the root*/
		else{

			/*current node initially takes the value of the root*/
			TLDNode *current_node = (TLDNode *)(elms->elements[0]);
			TLDNode *parent_node = NULL;

			while(current_node != NULL){
				parent_node = current_node;
				if(strcasecmp(current_node->k, tldcpy) < 0){
					/*go to the right subtree*/
					current_node = current_node->rc;
				}
				else if(strcasecmp(current_node->k, tldcpy) > 0){
					/*go to the left subtree*/
					current_node = current_node->lc;
				}
				else{

					/*
					node already exist, this won't happen, since the look_up function will tell us if the node already exists
					but put it here just in case the user uses it without using look_up before hand
					*/
					free(tldcpy);
					return 0;
				}
			}

			/*
			when we exit the while loop,
			the current node is null and
			we have found the place to insert the new node
			*/
			current_node = (TLDNode *)malloc(sizeof(TLDNode));
			if(current_node != NULL){
				current_node->p = parent_node;

				if(strcasecmp(parent_node->k, tldcpy) > 0){

					parent_node->lc = current_node;
				}
				else if(strcasecmp(parent_node->k, tldcpy) < 0){

					parent_node->rc = current_node;
				}
				else{
					free(tldcpy);
					return 0;
				}
				current_node->k = tldcpy;
				current_node->v = v;
				current_node->lc = NULL;
				current_node->rc = NULL;

				/*putting the node in the elements array*/
				elms->elements[elms->size] = current_node;
				elms->size += 1L;
			}
			else{
				free(tldcpy);
				fprintf(stderr, "Failed to allocate memory to node\n");
				return 0;
			}
		}
		
	}
	return 1;
}

static int bst_reassign(const TLDMap *tld, char *theTLD, long v){

	Elements *elm = (Elements *)(tld->self);
	TLDNode *current_node = elm->elements[0];

	while(current_node){
		if(strcasecmp(current_node->k, theTLD) > 0){
			current_node = current_node->lc;
		}
		else if(strcasecmp(current_node->k, theTLD) < 0){
			current_node = current_node->rc;
		}
		else{
			/*found the node, reassign its value */
			current_node->v = v;
			return 1;
		}
	}
	return 0;
}

static int bst_lookup(const TLDMap *tld, char *theTLD, long *v){
	
	Elements *elm = (Elements *)(tld->self);
	TLDNode *current_node = elm->elements[0];

	while(current_node){
		if(strcasecmp(current_node->k, theTLD) > 0){
			current_node = current_node->lc;
		}
		else if(strcasecmp(current_node->k, theTLD) < 0){
			current_node = current_node->rc;
		}
		else{
			/*found the node, put its value in *v */
			*v = current_node->v;
			return 1;
		}
	}
	return 0;
}

static const Iterator *bst_itCreate(const TLDMap *tld){
	Elements *elms = (Elements *)(tld->self);
	const Iterator *bst_iterator = NULL;
	bst_iterator = Iterator_create(elms->size, elms->elements);
	return bst_iterator;
}

const TLDMap* TLDMap_create(void){

	TLDMap *bst = (TLDMap *)malloc(sizeof(TLDMap));
	if(bst != NULL){

		/*Allocate memory to the elements struct to set TLDMap's "self" vraiable to*/
		Elements *elms = (Elements *)malloc(sizeof(Elements));
		if(elms != NULL){

			elms->size = 0L;
			elms->capacity = MIN_CAPACITY;
			/*make elements array for the iterator to use*/
			/*allocate capacity * size of nodes to the the elements array; because elements array contains nodes*/
			void **tmp = (void **)malloc((elms->capacity)*sizeof(TLDNode));
			if(tmp != NULL){
				elms->elements = tmp;

				/*Initializing the first element so valgrind won't yell at us for uninitialized value
				because the first element is going to be checked in lookup*/
				elms->elements[0] = root;
				/*assigning fucntions to TLDMaps func pointers*/
				bst->self = elms;
				bst->destroy = bst_destroy;
				bst->insert = bst_insert;
				bst->reassign = bst_reassign;
				bst->lookup = bst_lookup;
				bst->itCreate = bst_itCreate;

			} 
			else{
				free(elms);
				free(bst);
				bst = NULL;
			}
		}
		else{
			free(bst);
			bst = NULL;
		}
	}
	return bst;
}

char *TLDNode_tldname(TLDNode *node){
	return node->k;
}

long TLDNode_count(TLDNode *node){
	return node->v;
}